
package fr.test.leboncoin.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import fr.test.leboncoin.data.LeBonCoinDatabase
import fr.test.leboncoin.data.repository.AlbumRepositoryImpl
import fr.test.leboncoin.ui.viewmodels.MainViewModel
import fr.test.leboncoin.utilities.getValue
import org.junit.*

class MainViewModelTest {

    private lateinit var appDatabase: LeBonCoinDatabase
    private lateinit var viewModel: MainViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, LeBonCoinDatabase::class.java).build()
        val albumRepository = AlbumRepositoryImpl.getInstance(appDatabase.albumDao())
        viewModel = MainViewModel(context, albumRepository, null)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun testDefaultValues() {
        assert(getValue(viewModel.isListVisible))
    }
}