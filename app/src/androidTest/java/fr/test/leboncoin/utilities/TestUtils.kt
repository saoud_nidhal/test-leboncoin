package fr.test.leboncoin.utilities

import fr.test.leboncoin.data.Album

val testAlbums = arrayListOf(
    Album(1, 1, "https://via.placeholder.com/600/92c952", "accusamus beatae ad facilis cum similique qui sunt","https://via.placeholder.com/150/92c952"),
    Album(2, 100, "https://via.placeholder.com/600/92c952", "reprehenderit est deserunt velit ipsam","https://via.placeholder.com/150/92c952"),
    Album(3, 200, "https://via.placeholder.com/600/92c952", "officia porro iure quia iusto qui ipsa ut modi","https://via.placeholder.com/150/92c952"),
    Album(4, 300, "https://via.placeholder.com/600/92c952", "culpa odio esse rerum omnis laboriosam voluptate repudiandae","https://via.placeholder.com/150/92c952"),
    Album(5, 500, "https://via.placeholder.com/600/92c952", "culpa odio esse rerum omnis laboriosam voluptate repudiandae","https://via.placeholder.com/150/92c952"),
    Album(6, 500, "https://via.placeholder.com/600/92c952", "culpa odio esse rerum omnis laboriosam voluptate repudiandae","https://via.placeholder.com/150/92c952")
)
val testAlbum = testAlbums[0]