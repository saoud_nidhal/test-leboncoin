package fr.test.leboncoin


import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import fr.test.leboncoin.ui.MainActivity
import fr.test.leboncoin.ui.adapters.AlbumAdapter
import fr.test.leboncoin.utilities.testAlbums
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class MainActivityTest {
  @Rule
  @JvmField
  var activityTestRule = ActivityTestRule(MainActivity::class.java)

  private lateinit var mAdapter : AlbumAdapter

  @Before
  fun init() {
    val context = InstrumentationRegistry.getInstrumentation().targetContext
    mAdapter = AlbumAdapter()
    mAdapter.submitList(testAlbums)

    activityTestRule.runOnUiThread{
      val recyclerView = activityTestRule.activity.findViewById<RecyclerView>(R.id.album_list)
      val textError = activityTestRule.activity.findViewById<TextView>(R.id.data_text_error)
      recyclerView.apply {
        adapter = mAdapter
        visibility = View.VISIBLE
      }
      textError.visibility = View.GONE
    }
  }

  @Test
  fun checkIfRecycleViewIsVisible() {
    Espresso.onView(ViewMatchers.withId(R.id.album_list))
      .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
  }

  @Test
  fun checkScrollRecycleview() {
    val itemCount = mAdapter.itemCount
    Espresso.onView(ViewMatchers.withId(R.id.album_list))
      .inRoot(
        RootMatchers.withDecorView(Matchers.`is`(activityTestRule.activity.window.decorView))
      )
      .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemCount / 2 - 1))
  }


  @Test
  fun testClickItem() {
    Espresso.onView(ViewMatchers.withId(R.id.album_list))
      .inRoot(
        RootMatchers.withDecorView(
          Matchers.`is`(activityTestRule.activity.window.decorView)
        )
      )
      .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
  }
}
