package fr.test.leboncoin.data.repository

import fr.test.leboncoin.data.Album
import fr.test.leboncoin.data.AlbumDao
import io.reactivex.Completable
import io.reactivex.Single

class AlbumRepositoryImpl constructor(private val albumDao: AlbumDao) :
  AlbumRepository {

  override fun saveAlbums(albums: List<Album>) : Completable {
    return albumDao.insertAll(albums)
  }

  override fun getAlbums() : Single<List<Album>> {
   return albumDao.getAlbums()
  }
  companion object {
    @Volatile private var instance: AlbumRepository? = null
    fun getInstance(albumDao: AlbumDao) =
      instance ?: synchronized(this) {
        instance
          ?: AlbumRepositoryImpl(albumDao).also { instance = it }
      }
  }
}
