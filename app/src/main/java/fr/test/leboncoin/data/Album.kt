package fr.test.leboncoin.data


import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "album")
data class Album(
    @PrimaryKey val id: Int,
    val albumId: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
) {
    override fun toString() = title
}