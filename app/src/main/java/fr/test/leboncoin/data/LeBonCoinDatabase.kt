package fr.test.leboncoin.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import fr.test.leboncoin.utilities.DATABASE_NAME

@Database(entities = [Album::class], version = 1, exportSchema = false)
abstract class LeBonCoinDatabase: RoomDatabase()  {
  abstract fun albumDao(): AlbumDao
  companion object {
    @Volatile private var instance: LeBonCoinDatabase? = null
    fun getInstance(context: Context): LeBonCoinDatabase {
      return instance ?: synchronized(this) {
        instance
            ?: buildDatabase(context).also { instance = it }
      }
    }

    private fun buildDatabase(context: Context): LeBonCoinDatabase {
      return Room.databaseBuilder(context, LeBonCoinDatabase::class.java,
          DATABASE_NAME
      )
        .build()
    }
  }
}
