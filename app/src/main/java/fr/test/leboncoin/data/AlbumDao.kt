package fr.test.leboncoin.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single


@Dao
interface AlbumDao {
    @Query("SELECT * FROM album order by id")
    fun getAlbums(): Single<List<Album>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     fun insertAll(plants: List<Album>) : Completable

    @Query("SELECT * FROM album WHERE id = :id")
    fun getAlbum(id : Int) : LiveData<Album>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDataWithSupension(plants: List<Album>)
}
