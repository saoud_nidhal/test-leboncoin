package fr.test.leboncoin.data.repository

import fr.test.leboncoin.data.Album
import io.reactivex.Completable
import io.reactivex.Single

interface AlbumRepository  {
  fun getAlbums() : Single<List<Album>>
  fun saveAlbums(albums : List<Album>) : Completable
}
