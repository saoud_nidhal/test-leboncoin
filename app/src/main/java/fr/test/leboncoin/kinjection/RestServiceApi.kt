package fr.test.leboncoin.kinjection

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import fr.test.leboncoin.BuildConfig
import fr.test.leboncoin.utilities.BASE_URL
import fr.test.leboncoin.utilities.TIMOUT_IN_SECONDES
import fr.test.leboncoin.utilities.rest.ApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    single { provideGson() }
    single { provideConverterFactory(get()) }
    single { provideHttpLogger() }
    single { provideOkHttp(get()) }
    single { provideRetrofit(get(), get()) }
    single { provideApiResponse(get()) }
}

fun provideGson(): Gson {
    val gsonBuilder = GsonBuilder()
        .setLenient()
        .create()
    return gsonBuilder
}

fun provideConverterFactory(gson: Gson): Converter.Factory {
    return GsonConverterFactory.create(gson)

}

fun provideHttpLogger(): HttpLoggingInterceptor {
    val logging = HttpLoggingInterceptor()
    logging.level =
        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    return logging
}

fun provideOkHttp(
    httpLoggingInterceptor: HttpLoggingInterceptor
): OkHttpClient {
    var okHttpClient = OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .connectTimeout(TIMOUT_IN_SECONDES, TimeUnit.SECONDS)
        .readTimeout(TIMOUT_IN_SECONDES, TimeUnit.SECONDS)
    return okHttpClient.build()
}

fun provideRetrofit(
    converterFactory: Converter.Factory,
    okHttpClient: OkHttpClient
): Retrofit {
    var retrofit = Retrofit.Builder()
        .addConverterFactory(converterFactory)
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .baseUrl(BASE_URL)
        .client(okHttpClient)
    return retrofit.build()
}

fun provideApiResponse(
    retorfit: Retrofit
): ApiService {
    return retorfit.create(ApiService::class.java)
}
