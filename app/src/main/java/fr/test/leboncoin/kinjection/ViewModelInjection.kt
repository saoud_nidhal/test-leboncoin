package fr.test.leboncoin.kinjection

import fr.test.leboncoin.data.LeBonCoinDatabase
import fr.test.leboncoin.data.repository.AlbumRepositoryImpl
import fr.test.leboncoin.ui.viewmodels.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
  single { LeBonCoinDatabase.getInstance(get()).albumDao() }
  single { AlbumRepositoryImpl.getInstance(get()) }
  viewModel { MainViewModel(get(),get(), get()) }
}

