package fr.test.leboncoin.ui.viewmodels

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.test.leboncoin.data.Album
import fr.test.leboncoin.data.repository.AlbumRepository
import fr.test.leboncoin.utilities.checkInternetConnection
import fr.test.leboncoin.utilities.rest.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.launch
import retrofit2.Response
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers.io

class MainViewModel internal constructor(
  private val context: Context,
  private val albumRepository: AlbumRepository,
  private val api: ApiService?
) : ViewModel() {
  var albums: MutableLiveData<List<Album>> = MutableLiveData(listOf())
  var isListVisible : MutableLiveData<Boolean> = MutableLiveData(true)
  private val mDisposable = CompositeDisposable()
  fun getListPlant() {
    when (checkInternetConnection(context)) {
      true -> getAlbums()
      else ->getAlbumsFromDatabase()
    }
  }

  private fun getAlbums() {
    viewModelScope.launch {
      try {
        api?.let {
          onSuccesGetAlbumsCallback(it.getAlbums().await())
        }
      } catch (e: Exception) {
        getAlbumsFromDatabase()
        e.printStackTrace()
      }
    }
  }

  private fun getAlbumsFromDatabase(){
    mDisposable.add(albumRepository.getAlbums().subscribeOn(io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe({ albums.postValue(it) },
        { throwable ->
          isListVisible.postValue(false)
          Log.e(
            MainViewModel::class.simpleName,
            "Unable to get albums",
            throwable
          )
        }))
  }
  fun clearDisposable() {
    mDisposable.clear()
  }

  private fun onSuccesGetAlbumsCallback(response: Response<List<Album>>) {
    response.body()?.let {
      mDisposable.add(
        albumRepository.saveAlbums(it).subscribeOn(io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ albums.postValue(it) },
            { throwable ->
              Log.e(
                MainViewModel::class.simpleName,
                "Unable to save albums",
                throwable
              )
            })
      )
    }
  }
}
