package fr.test.leboncoin.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.test.leboncoin.data.Album
import fr.test.leboncoin.databinding.ListItemAlbumBinding

class AlbumAdapter : ListAdapter<Album, RecyclerView.ViewHolder>(AlbumDiffCallback()) {
  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val album = getItem(position)
    (holder as AlbumViewHolder).bind(album)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    return AlbumViewHolder(
      ListItemAlbumBinding.inflate(
        LayoutInflater.from(parent.context), parent, false
      )
    )
  }

  class AlbumViewHolder(
    private val binding: ListItemAlbumBinding
  ) : RecyclerView.ViewHolder(binding.root) {
    init {
      binding.setClickListener {
        binding.album?.let { album ->
          Toast.makeText(itemView.context, album.title, Toast.LENGTH_LONG).show()
        }
      }
    }

    fun bind(item: Album) {
      binding.apply {
        album = item
        executePendingBindings()
      }
    }
  }
}

private class AlbumDiffCallback : DiffUtil.ItemCallback<Album>() {
  override fun areItemsTheSame(oldItem: Album, newItem: Album): Boolean {
    return oldItem.id == newItem.id
  }

  override fun areContentsTheSame(oldItem: Album, newItem: Album): Boolean {
    return oldItem == newItem
  }
}
