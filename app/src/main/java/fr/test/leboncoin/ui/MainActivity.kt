package fr.test.leboncoin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.observe
import fr.test.leboncoin.R
import fr.test.leboncoin.databinding.ActivityMainBinding
import fr.test.leboncoin.ui.adapters.AlbumAdapter
import fr.test.leboncoin.ui.viewmodels.MainViewModel
import fr.test.leboncoin.utilities.BUNDLE_MAIN_CHARGING
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val mainViewModel: MainViewModel by viewModel()
    private lateinit var mAdapter: AlbumAdapter
    private lateinit var binding: ActivityMainBinding
    private var isDataAlreadyAvailable = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mAdapter = AlbumAdapter()
        binding.albumList.adapter = mAdapter
        subscribeUiObserver()
        savedInstanceState?.let {
            isDataAlreadyAvailable = it.getBoolean(BUNDLE_MAIN_CHARGING, false)
        }
    }

    override fun onStart() {
        super.onStart()
        if (!isDataAlreadyAvailable)
            mainViewModel.getListPlant()
    }

    override fun onStop() {
        super.onStop()
        mainViewModel.clearDisposable()
    }

    private fun subscribeUiObserver() {
        mainViewModel.albums.observe(this@MainActivity) { its ->
            binding.hideList = !its.isNullOrEmpty()
            mAdapter.submitList(its)
        }
        mainViewModel.isListVisible.observe(this@MainActivity) { visible ->
            binding.hideList = visible
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(BUNDLE_MAIN_CHARGING, mAdapter.itemCount > 0)
    }
}
