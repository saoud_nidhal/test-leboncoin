package fr.test.leboncoin.utilities.rest

import fr.test.leboncoin.data.Album
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("technical-test.json")
    fun getAlbums(): Deferred<Response<List<Album>>>
}