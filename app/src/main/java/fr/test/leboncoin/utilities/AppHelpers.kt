package fr.test.leboncoin.utilities

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

fun checkInternetConnection(context : Context) :Boolean{
  val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
  val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
  return activeNetwork?.isConnected == true
}
