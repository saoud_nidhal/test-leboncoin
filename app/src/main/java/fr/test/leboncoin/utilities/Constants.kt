package fr.test.leboncoin.utilities


const val DATABASE_NAME = "leboncoin-db"
const val BASE_URL = "https://static.leboncoin.fr/img/shared/"
const val TIMOUT_IN_SECONDES: Long = 60
const val BUNDLE_MAIN_CHARGING = "BUNDLE_MAIN_CHARGING"
