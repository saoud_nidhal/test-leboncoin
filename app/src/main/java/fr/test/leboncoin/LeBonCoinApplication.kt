package fr.test.leboncoin

import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import fr.test.leboncoin.kinjection.mainModule
import fr.test.leboncoin.kinjection.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LeBonCoinApplication: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this);
        startKoin {
            // Android context
            androidContext(this@LeBonCoinApplication)
            // modules
            modules(listOf(
                networkModule,
                mainModule
            ))
        }
    }
}