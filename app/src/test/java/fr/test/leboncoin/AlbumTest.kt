package fr.test.leboncoin

import fr.test.leboncoin.data.Album
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class AlbumTest {
  private lateinit var album: Album
  @Before
  fun setUp() {
    album = Album(
      1,
      100,
      "https://via.placeholder.com/150/92c952",
      "accusamus beatae ad facilis cum similique qui sunt",
      "https://via.placeholder.com/600/92c952"
    )
  }

  @Test
  fun test_default_values() {
    Assert.assertEquals(1, album.id)
    Assert.assertEquals(100, album.albumId)
    Assert.assertEquals("https://via.placeholder.com/150/92c952", album.thumbnailUrl)
    Assert.assertEquals("accusamus beatae ad facilis cum similique qui sunt", album.title)
  }

  @Test
  fun test_isHttpsPictureUrl() {
    Assert.assertEquals("https", album.thumbnailUrl.split("://")[0])
  }

  @Test
  fun test_toString() {
    Assert.assertEquals("accusamus beatae ad facilis cum similique qui sunt", album.toString())
  }
}
